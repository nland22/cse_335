\documentclass[english 9pt]{extarticle}

\usepackage[letterpaper, margin=.3in]{geometry} %% Set up the margins
\usepackage[tiny]{titlesec} %% Smaller sections
\usepackage{minted} %% Syntax highlighting
\usepackage{multicol} %% Mulitple columns
\usepackage[T1]{fontenc}
\usepackage{etoolbox}
\usepackage{graphicx}
\AtBeginEnvironment{minted}{\singlespacing%
    \fontsize{7}{7}\selectfont}
\usepackage{inconsolata}
\setlength{\columnsep}{1cm} %% Distance between columns

\begin{document}

\begin{multicols*}{2}

  \section*{\textit{sllgen}}

  \begin{description}

  \item[Scanning] is the process of dividing the sequence of
    characters into words, punctuation, etc\ldots These units are
    called \textit{lexical items, lexemes,} or most often
    \textit{tokens}.

  \item[Parsing] is the process of organizing the sequences of tokens
    into hierarchical syntactic structures such as expressions,
    statements and blocks. This is like organizing or diagramming a
    sentence into clauses.

  \end{description}

  \section*{Section 3.1 - Specification and Implementation Strategy}

  \paragraph{}
  Specifications will be as follows:\\

  (value-of $exp$ $p$) = $val$\\

  We will have the following rules:

  \begin{tabbing}
    \textit{Expression} \= ::= \= \kill
    \textit{Program}    \> ::= \> \textit{Expression} \\
                        \>     \> \fbox{\ttfamily a-program (exp1)} \\
    \textit{Expression} \> ::= \> \textit{Number} \\
                        \>     \> \fbox{\ttfamily const-exp (num)} \\
    \textit{Expression} \> ::= \> -(\textit{Expression, Expression}) \\
                        \>     \> \fbox{\ttfamily diff-exp (exp1
                          exp2)} \\
    \textit{Expression} \> ::= \> zero? \textit{Expression} \\
                        \>     \> \fbox{\ttfamily zero?-exp (exp1)} \\
    \textit{Expression} \> ::= \> if \textit{Expression} then
                        \textit{Expression} else \textit{Expression} \\
                        \>     \> \fbox{\ttfamily if-exp (exp1 exp2 exp3)} \\
    \textit{Expression} \> ::= \> \textit{Identifier} \\
                        \>     \> \fbox{\ttfamily var-exp (var)} \\
    \textit{Expression} \> ::= \> let \textit{Identifier} =
                                  \textit{Expression} in \textit{Expression}\\
                        \>     \> \fbox{\ttfamily let-exp (var exp1 body)} \\
  \end{tabbing}

  \paragraph{}
  The flow goes like this:

  \begin{minted}{racket}
    (run "fun() = up(42)")
  \end{minted}

  Gets run through the \ttfamily{run} function Which results in the following
  \textit{ast} being created

  \begin{minted}{racket}
    (a-program '() (fun-expr '() (up-expr (num-expr 42))) '())
    (fun-expr '() (up-expr (num-expr 42)))
  \end{minted}

  Here we can see that the final desired output is:

  \begin{minted}{racket}
    (fun-expr '() (up-expr (num-expr 42)))
  \end{minted}

  Which tells us that we passed a function with an empty
  \ttfamily{env} and we set that equal to an \ttfamily{up-expr} with a
  \ttfamily{num-expr 42}. All of this really gets generated at
  \ttfamily{value-of}, that is the big player in this interpreter.

  \section*{Description of HW 8 Solutions}

  \begin{minted}{racket}
    (fun-expr
     (vars-expr body-expr)
     (proc-val (procedure vars-expr body-expr env))
     )

    ;$solution
    (fun-call-expr
     (fun-exp argv-expr)
     (letrec
       ([fun (proc-val->proc (value-of fun-exp env))]
        [var-names (proc->vars fun)]
        ;$solution we evaluate the values
        ;of the parameters in the current environment
        [argument-values (map (lambda (x) (value-of x env)) argv-expr)]
        [fun-body (proc->body fun)]
        [proc-env (proc->env fun)]
        ;$solution we extend the environment in which the
        ;function was created with all the paramater
        ; names -> values pairs.
        [new-env
         (begin
           (or (equal? (length var-names) (length argument-values))
             (raise (~a "arity mismatch. expected "(length var-names) "
             arguments, received " (length argument-values))))
           (extend-env-multiple-times var-names argument-values proc-env FINAL)
           )])

       ;$solution we evaluate the body of the procedure in this new environment.
       (value-of fun-body new-env)
       )
     )
  \end{minted}

  In the previous code block, wee need to extract all the parts of the
  function. So, we'll use \ttfamily{letrec} to extract some of the
  data out. There are extractors on \ttfamily{fun},
  \ttfamily{var-names}, \ttfamily{argument-values},
  \ttfamily{fun-body}, \& \ttfamily{proc-env}. We use these extracted
  values to create a new \ttfamily{env}. Once the new \ttfamily{env}
  is created, we then pass the \ttfamily{fun-body} \&
  \ttfamily{new-env} to \ttfamily{value-of}, and we already know that
  \ttfamily{value-of} will give us the desired results.

  \section*{Typing Rules}

  Type values belong to a \textbf{different category of values}.\\
  Typing rules are equations.

  \subsection*{Type Inference}

  \textbf{What is the type of}

  \[ proc\ (f)\ proc(x)\ - ((f3), (fx)) \]

  \begin{itemize}
  \item Introduce a type variable for each:
    \begin{itemize}
    \item Sub-expression
    \item Bound Variable
    \end{itemize}
  \item Generate the constraints for each sub-expression
    \begin{itemize}
    \item Based on its typing rule
    \end{itemize}
  \item Solve the equations
  \end{itemize}

  %% Initial Table
  \begin{tabular}{|l|l|}
    \hline
    \multicolumn{1}{|c|}{\textbf{(Sub)Expression}} & \textbf{Type Variable} \\
    \hline
    f & T_f\\
    \hline
    x & t_x\\
    \hline
    proc (f) proc(x) -((f3)(fx)) & T_0\\
    \hline
    proc(x) -((f3)(fx)) & T_1\\
    \hline
    -((f3)(fx)) & T_2\\
    \hline
    (f3) & T_3\\
    \hline
    (fx) & T_4\\
    \hline
  \end{tabular}

  \scalebox{0.85}{%
    \begin{tabular}{|l|l|l|}
      \hline
      \multicolumn{1}{|c|}{\textbf{Expression}} & \textbf{Type Variable}
      & \textbf{Equations} \\
      \hline
      f & T_f &\\
      \hline
      x & T_x &\\
      \hline
      proc (f) proc(x) -((f3)(fx)) & T_0 & T_0 = T_f \rightarrow T_1\\
      \hline
      proc(x) -((f3)(fx)) & T_1 & T_1 = T_x \rightarrow T_2\\
      \hline
      -((f3)(fx)) & T_2 & \shortstack{T_3 = int \\ T_4 = int \\ T_2 = int} \\
      \hline
      (f3) & T_3 & T_f = int \rightarrow T_3\\
      \hline
      (fx) & T_4 & T_f = T_x \rightarrow T_4\\
      \hline
  \end{tabular}} \\

  \textbf{Some Terms}

  \begin{itemize}
    \item \textbf{Solution}
      \begin{itemize}
        \item Set of values for type variables that satisfy ALL equations
      \end{itemize}
    \item \textbf{Substitution}
      \begin{itemize}
        \item One equation with its LHS being a variable, e.g.\, $T_3\ =\ int$
      \end{itemize}
    \item \textbf{Unification}
      \begin{itemize}
        \item Systematically solving the equations, e.g.\, computing of substitutions.
      \end{itemize}
  \end{itemize}

  \textbf{Unification} \\

  \textbf{Step 1} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    T_0 = T_f \rightarrow T_1 & \\
    \hline
    T_1 = T_x \rightarrow T_2 & \\
    \hline
    \shortstack{T_3 = int \\ T_4 = int \\ T_2 = int} & \\
    \hline
    T_f = int \rightarrow T_3 & \\
    \hline
    T_f = T_x \rightarrow T_4 & \\
    \hline
  \end{tabular}\\

  \textbf{Step 2} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = T_f \rightarrow \fbox{T_1} \\
    \hline
    & \fbox{T_1} = T_x \rightarrow T_2 \\
    \hline
    \shortstack{T_3 = int \\ T_4 = int \\ T_2 = int} & \\
    \hline
    T_f = int \rightarrow T_3 & \\
    \hline
    T_f = T_x \rightarrow T_4 & \\
    \hline
  \end{tabular}\\

  \textbf{Step 3} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = T_f \rightarrow (\fbox{T_x \rightarrow T_2}) \\
    \hline
    & \fbox{T_1} = T_x \rightarrow T_2 \\
    \hline
    \shortstack{T_3 = int \\ T_4 = int \\ T_2 = int} & \\
    \hline
    T_f = int \rightarrow T_3 & \\
    \hline
    T_f = T_x \rightarrow T_4 & \\
    \hline
  \end{tabular}\\

  \textbf{Step 4} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = T_f \rightarrow (T_x \rightarrow \fbox{T_2})\\
    \hline
    & T_1 = T_x \rightarrow \fbox{T_2} \\
    \hline
    & \shortstack{T_3 = int \\ T_4 = int \\ \fbox{T_2} = int} \\
    \hline
    T_f = int \rightarrow T_3 & \\
    \hline
    T_f = T_x \rightarrow T_4 & \\
    \hline
  \end{tabular}\\

  \textbf{Step 5} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = T_f \rightarrow (T_x \rightarrow \fbox{int})\\
    \hline
    & T_1 = T_x \rightarrow \fbox{int} \\
    \hline
    & \shortstack{T_3 = int \\ T_4 = int \\ \fbox{T_2} = int} \\
    \hline
    T_f = int \rightarrow T_3 & \\
    \hline
    T_f = T_x \rightarrow T_4 & \\
    \hline
  \end{tabular}\\

  \textbf{Step 6} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = T_f \rightarrow (T_x \rightarrow int)\\
    \hline
    & T_1 = T_x \rightarrow int \\
    \hline
    & \shortstack{\fbox{T_3} = int \\ T_4 = int \\ T_2 = int} \\
    \hline
    T_f = int \rightarrow \fbox{T_3} & \\
    \hline
    T_f = T_x \rightarrow T_4 & \\
    \hline
  \end{tabular}\\

  \textbf{Step 7} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = \fbox{T_f} \rightarrow (T_x \rightarrow int) \\
    \hline
    & T_1 = T_x \rightarrow int \\
    \hline
    & \shortstack{T_3 = int \\ T_4 = int \\ T_2 = int} \\
    \hline
    & \fbox{T_f} = int \rightarrow int \\
    \hline
    T_f = T_x \rightarrow T_4 & \\
    \hline
  \end{tabular}\\

  \textbf{Step 8} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = \fbox{(int \rightarrow int)} \rightarrow (T_x \rightarrow int) \\
    \hline
    & T_1 = T_x \rightarrow int \\
    \hline
    & \shortstack{T_3 = int \\ T_4 = int \\ T_2 = int} \\
    \hline
    & \fbox{T_f} = int \rightarrow int \\
    \hline
    \fbox{T_f} = T_x \rightarrow T_4 & \\
    \hline
  \end{tabular}\\

  \textbf{Step 9} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = (int \rightarrow int) \rightarrow (T_x \rightarrow int) \\
    \hline
    & T_1 = T_x \rightarrow int \\
    \hline
    & \shortstack{T_3 = int \\ \fbox{T_4} = int \\ T_2 = int} \\
    \hline
    & T_f = int \rightarrow int \\
    \hline
    T_f = T_x \rightarrow \fbox{T_4} & \\
    \hline
  \end{tabular}\\

  \textbf{Step 10} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = (int \rightarrow int) \rightarrow (T_x \rightarrow int) \\
    \hline
    & T_1 = T_x \rightarrow int \\
    \hline
    & \shortstack{T_3 = int \\ T_4 = int \\ T_2 = int} \\
    \hline
    & T_f = int \rightarrow int \\
    \hline
    int \rightarrow int = T_x \rightarrow int & \\
    \hline
  \end{tabular}\\

  \textbf{Step 11} \\
  \begin{tabular}{|l|l|}
    \hline
    \textbf{Equations} & \textbf{Substitutions}\\
    \hline
    & T_0 = (int \rightarrow int) \rightarrow (int \rightarrow int) \\
    \hline
    & T_1 = T_x \rightarrow int \\
    \hline
    & \shortstack{T_3 = int \\ T_4 = int \\ T_2 = int} \\
    \hline
    & T_f = int \rightarrow int \\
    \hline
    & T_x = int\\
    \hline
  \end{tabular}\\

  \textbf{This means that} \\
  $proc\ (f)\ proc(x)\ -((f\ 3)(f\ x))$
  \Rightarrow (int \rightarrow int) \rightarrow (int \rightarrow int)

\end{multicols*}
\end{document}
