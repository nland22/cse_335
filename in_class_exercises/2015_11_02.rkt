#lang racket

; Evn ::= (empty-env)
;      | (extend-env var val Env)

(#%require (lib "eopl.ss" "eopl"))

(define-datatype Env Env?
  (empty-env)                 ; Can be this
  (extend-env (var symbol?)   ; Or this, which is a symbol, number, and env
              (val number?)
              (env Env?)
  ))

(define env1 (empty-env))
(define env2 (extend-env 'x 2 env1))
(define given-env (extend-env 'x 2 env1))

(Env? env1) ; #t
(Env? env2) ; #t
;(empty-env? env2) ; error

(define (env-lookup search-var given-env)
  (cases Env given-env
    (empty-env  () "Not found!")
    (extend-env (var val env)
      (if (eqv? search-var var)
        val
        (env-lookup search-var env)))))

(env-lookup 'x given-env) ; 2
(env-lookup 'z given-env) ; "Not found!"

; Expr ::= num                const-expr
;       |  Expr Expr Expr-*  ;complex-expr

(define-datatype Expr Expr?
  (const-expr (num number?)
  (complex-expr (expr Expr?)
                (expr2 Expr?)
                (expr-* (list-of Expr?)))))

((list-of number?) '(1 2 3)) ; #t

(Expr? 1)
