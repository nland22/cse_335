#lang racket

;; lambda f(x y) = (x + y)
(define add
  (lambda (x y) (+ x y)))

(add 4 100) ;; 104
(add 100 100) ;; 200

(define (add-proc y)
  (lambda (x) (+ x y)))

((add-proc 1) 100) ;; 101
((add-proc 100) 100) ;; 200