#lang racket
(#%provide (all-defined))

#|
IMPORTANT:
Overall, you are allowed to change this file in any way that does *not* affect the
compilation of the accompanying test file. Changes that are almost certain to break
the above restriction are:
  - changing the names of any definitions that are explicitely used in the tests
    (e.g. function names, relevant constants)

If there are any specific instructions for a problem, please read them carefully. Otherwise,
follow these general rules:

   - replace the 'UNIMPLEMENTED symbol with your solution
   - you are NOT allowed to change the number of arguments of the pre-defined functions,
     because changing the number of arguments automatically changes the semantics of the
     function. Changing the name of the arguments is permitted since that change only
     affects the readability of the function, not the semantics.
   - you may write any number of helper functions

When done, make sure that the accompanying test file compiles.
If you cannot come up with a correct solution then please make the answer-sheet
compiles. If you have partial solutions that do not compile please comment them out,
if this is the case, the default definitions will have to be present since the tests
will be expecting functions with the names defined here.

Submission guidelines:
   - please rename the file to hw04-yourlastname-answer.rkt prior to submission
   - only the renamed file file needs to be uploaded
|#
;======================================01=======================================
(define-syntax-rule (for {var <- value-range} yield result)
  (map (lambda (var) result) value-range))

;(for {i <- '(0 1 5)} yield (+ i 42))
;======================================02=======================================
(define-syntax-rule (seq expr1 expr2)
  ((lambda () expr1 expr2)))

;(define test-var-1 42)

;(and
;   (equal? (seq (set! test-var-1 'not-42) 342) 342)
;   (equal? test-var-1 'not-42))

;(seq 'to-be-ignored (* 5 6))

;====
(define-syntax-rule (while condition body)
  (let loop ()
    (when condition
      body
      (loop)) 0))

;(define x 0)
;(while (< x 100) (set! x (+ x 1)))
;x
;(while (< x 105) (seq (display " * ") (set! x (+ x 1))))
;======================================03=======================================

#|
<step> ::=  <step>  <step>       "seq-step"
          | "up" number          "up-step"
          | "down" number        "down-step"
          | "left" number        "left-step"
          | "right" number       "right-step"
|#
;example of how to create the error message for the "up-step" constructor
;> (invalid-args-msg "up-step" "number?" '(1 2 3 4))
;where '(1 2 3 4) should be replaced by the actual violating value.

;;you can reorder the functions below if it better suits your needs
(define (up-step n)
  (if (number? n) (list "up-step" n)
  (raise (string-append "Invalid arguments in: up-step --- expected: number? --- received: " n)))
)


(define (down-step n)
  (if (number? n) (list "down-step" n)
  (raise (string-append "Invalid arguments in: down-step --- expected: number? --- received: " n)))
)

(define (left-step n)
  (if (number? n) (list "left-step" n)
  (raise (string-append "Invalid arguments in: left-step --- expected: number? --- received: " n)))
)

(define (right-step n)
  (if (number? n) (list "right-step" n)
  (raise (string-append "Invalid arguments in: right-step --- expected: number? --- received: " n)))
)

(define (seq-step st-1 st-2)
  (if (and (step? st-1) (step? st-2)) (list "seq-step" st-1 st-2)
  (raise (string-append "Invalid arguments in: seq-step --- expected: step? --- received: " st-1)))
)

;;====
(define (up-step? st)
  (if (eq? (car st) "up-step") #t
  #f)
)

(define (down-step? st)
  (if (eq? (car st) "down-step") #t
  #f)
)

(define (left-step? st)
  (if (eq? (car st) "left-step") #t
  #f)
)

(define (right-step? st)
  (if (eq? (car st) "right-step") #t
  #f)
)

(define (seq-step? st)
  (step? (and (car (cdr st)) (car (cddr st))))
)

;This is a predicate that tells you whether or not something is a step,
;it should return true when given either up, down, left, right or seq steps.
(define (step? st)
  (if (pair? st)
    (cond
      [(up-step? st) #t]
      [(down-step? st) #t]
      [(left-step? st) #t]
      [(right-step? st) #t]
      [(seq-step? st) #t]
      [else #f])
      #f)
)

;; to avoid needless duplication we will only implement one extractor to handle all the
;; simple steps, rather than four of them.
;; So this should take: up, down, left and right steps.
(define (single-step->n st)
  (if (step? st) (car (cdr st))
  (raise "Invalid arguments in: single-step->n --- expected: single-step? --- received: not-a-single-step"))
)

;;two extractors, one for each piece of data representing a sequential step
(define (seq-step->st-1 st)
  (car (cdr st))
)


(define (seq-step->st-2 st)
  (car (cddr st))
)

;;===================================
(define (move start-p step)
  (cond
    [(eq? (car step) "up-step") (list 0 (+ (car (cdr start-p)) (car(cdr step))))]
    [(eq? (car step) "down-step") (list 0 (- (car (cdr start-p)) (car(cdr step))))]
    [(eq? (car step) "left-step") (list (- (car (cdr start-p)) (car (cdr step))) 0)]
    [(eq? (car step) "right-step") (list (+ (car (cdr start-p)) (car (cdr step))) 0)]
    [else start-p]
    )
)

;(move '(0 0) (up-step 3))
;(move '(0 0) (down-step 3))
;(move '(0 0) (left-step 3))
;(move '(0 0) (right-step 3))
;======================================04=======================================
(define bound 100)

;singleton-set should return a function that takes a number as an argument and
;tells whether or not that number is in the set
(define (singleton-set x)
(lambda (num) (= num x))

)
;  (lambda (x) (= num x)))
;((singleton-set 2) 1)
;((singleton-set 2) 2)

;the set of all elements that are in either 's1' or 's2'
(define (union s1 s2)
  (lambda (x) (or
    (s1 x)
    (s2 x))))

;the set of all elements that are in both  in 's1' and 's2'
(define (intersection s1 s2)
  (lambda (x) (and
    (s1 x)
    (s2 x))))

;the set of all elements that are in 's1', but that are not in 's2'
(define (diff s1 s2)
  (lambda (x) (and
    (s1 x)
    (not(s2 x)))))

;returns the subset of s, for which the predicate 'predicate' is true.
(define (filter predicate s)
  ;'UNIMPLEMENTED
  (lambda (x)
    (if(predicate x)
    (s x)
    #f

    )
  )
  )

  ;(s1-7

;we assume that the sets can contain only numbers between 0 and bound



(define s1 (singleton-set 1))
(define s2 (singleton-set 2))
;((union s1 s2) 3)
(define s3 (singleton-set 3))
(define s4 (singleton-set 4))
(define s7 (singleton-set 7))
(define s13 (singleton-set 13))
;(define s4 (singleton-set 4))

(define primes (union (union s3 s7) s13))
(define mixed (union primes s4))


(define s1-7 (union (union (union (union s1 s2) s3) s4) s7))
;returns whether or not the set contains at least an element for which
;the predicate is true. s below is the parameter standing for a given set
(define (exists? predicate s)
  ;'UNIMPLEMENTED
  ;((filter predicate s) bound)
  (for/or ([i (in-range 1 bound)])
  (and (predicate i) (s i)))


  )

;returns whether or not the predicate is true for all the elements
;of the given set s
(define (all? predicate s)
  ;'UNIMPLEMENTED
  (not (for/or ([i (in-range 1 bound)])
  (and (not (predicate i)) (s i)))))




  ;just a sample predicate
  (define (prime? n)
    (define (non-divisible? n)
      (lambda (i)
        (not (= (modulo n i) 0))))
    (define range-of-prime-divisors (cddr (range (+ (integer-sqrt n) 1))))
    (if (equal? n 1)
        #f
        (andmap (non-divisible? n) range-of-prime-divisors)
        )
    )

  ;    (define range-of-prime-divisors (cddr (range (+ (integer-sqrt 100) 1))))
;(range-of-prime-divisors)
  ;(define primes-set (filter prime? s1-7))
  ;(primes-set 2)
  ;(primes-set 1)
;returns a new set where "op" has been applied to all elements
; NOTE: just because a procedure/function has the word "map" in it, it
;       doesn't mean you have to use map higher order function to implement it.
;       Map is a functional operation with well defined behavior that
;       is not tied to any implementation.
(define (map-set op s)
  'UNIMPLEMENTED
  )
(all? prime? mixed)
;(prime? 0)

;=====================================05====================================
; FYI:
;  to emphasize the procedural-based approach to implement "step" data type and to
;  contrast it with the data structure-based approach for "step" implementation
;  used in p3, here we add "-proc" to each corresponding function name.

;====p5-a================
(define (up-step-proc n)
'UNIMPLEMENTED

)

(define (down-step-proc n)
  'UNIMPLEMENTED
)

(define (left-step-proc n)
  'UNIMPLEMENTED
)

(define (right-step-proc n)
  'UNIMPLEMENTED
)

(define (seq-step-proc st-1 st-2)
  'UNIMPLEMENTED
)

;;====
(define (up-step-proc? st)
  'UNIMPLEMENTED
)

(define (down-step-proc? st)
  'UNIMPLEMENTED
)

(define (left-step-proc? st)
  'UNIMPLEMENTED
)

(define (right-step-proc? st)
  'UNIMPLEMENTED
)

(define (seq-step-proc? st)
  'UNIMPLEMENTED
)

;This is a predicate that tells you whether or not st is a step,
; it should return true when given either up, down, left, right or seq steps.
(define (step-proc? st)
  'UNIMPLEMENTED
)


;;to avoid needless duplication we will only implement one extractor to handle all the
;; simple steps, rather than four of them. So this should take: up, down, left and right
;; steps.
(define (single-step-proc->n st)
  'UNIMPLMENTED
)

;;two extractors
(define (seq-step-proc->st-1 st)
  'UNIMPLEMENTED
)


(define (seq-step-proc->st-2 st)
  'UNIMPLEMENTED
)
;;========p5-b
(define (move-proc start-p step-proc)
  'UNIMPLEMENTED
)
