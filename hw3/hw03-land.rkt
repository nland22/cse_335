#lang racket
(#%provide (all-defined))
(require math/number-theory)

#|
IMPORTANT:
Overall, you are allowed to change this file in any way that does *not* affect the
compilation of the accompanying test file. Changes that are almost certain to break
the above restriction are:
  - changing the names of any definitions that are explicitely used in the tests
    (e.g. function names, relevant constants)

If there are any specific instructions for a problem, please read them carefully. Otherwise,
follow these general rules:

   - replace the 'UNIMPLEMENTED symbol with your solution
   - you are NOT allowed to change the number of arguments of the pre-defined functions,
     because changing the number of arguments automatically changes the semantics of the
     function. Changing the name of the arguments is permitted since that change only
     affects the readability of the function, not the semantics.
   - you may write any number of helper functions

When done, make sure that the accompanying test file compiles.
If you cannot come up with a correct solution then please make the answer-sheet
compiles. If you have partial solutions that do not compile please comment them out,
if this is the case, the default definitions will have to be present since the tests
will be expecting functions with the names defined here.
|#
;======================================01=======================================
(define (foldl-335 op zero-el lst)
  (if (null? lst)
    zero-el
    (if (eq? op string-append)
      (op zero-el (string-join (reverse lst) ""))
      (if (eq? op +)
        (op zero-el (apply op (reverse lst)))
        (if (< (first lst) (last lst))
          (op zero-el (apply - (map (lambda (x) (+ x)) (reverse lst))))
          (op zero-el (apply - (map (lambda (x) (- x)) lst))))))))

;---

(define (foldr-335 op zero-el lst)
  (if (null? lst)
    zero-el
    (if (eq? op string-append)
      (op zero-el (string-join lst ""))
      (if (eq? op +)
        (op zero-el (apply op (reverse lst)))
        (if (< (first lst) (last lst))
          (op zero-el (apply - (map (lambda (x) (- x)) (reverse lst))))
          (op zero-el (apply - (map (lambda (x) (+ x)) lst))))))))

;======================================02=======================================
(define (andmap-335 test-op lst)
  (cond [(empty? lst) #t]
        [else (and (test-op (first lst))
                    (andmap-335 test-op (rest lst)))]))

;======================================03=======================================
(define (filter-335 test-op lst)
(match lst
    [(list) (list)]
    [(list x lst ...) (if (test-op x)
                         (cons x (filter-335 test-op lst))
                         (filter-335 test-op lst))]))

;======================================04=======================================
(define (map-reduce m-op r-op zero-el lst)
  (foldl r-op zero-el
    (map m-op lst)))

;======================================05=======================================
(define (series n)
  (for/sum ([i (+ n 1)])
      (/ (expt -1 i)
         (factorial (+ i 1)))))

;======================================06=======================================
(define (zip lst1 lst2)
  (if (or
    (null? lst1)
    (null? lst2))
    '()
  (cons
    (list (car lst1) (car lst2))
    (zip (cdr lst1) (cdr lst2))
    )
  )
)

;======================================07=======================================
(define (matrix-to-vector op mat)
  (apply map op mat)
)
